<?php
/**
 * @file Send emails from google App Engine infra structure to override the normal 
 * drupal workflow.
 */

require_once 'google/appengine/api/app_identity/AppIdentityService.php';
require_once 'google/appengine/api/mail/Message.php';

use google\appengine\api\app_identity\AppIdentityService;
use google\appengine\api\mail\Message;
use Exception;

class GAEMailer extends DefaultMailSystem implements MailSystemInterface {
  /**
   * Accept an e-mail message and store it in a variable.
   *
   * @param $message
   *   An e-mail message.
   */
  public function mail(array $message) {
    // $captured_emails = variable_get('drupal_test_email_collector', array());
    // $captured_emails[] = $message;
    // variable_set('drupal_test_email_collector', $captured_emails);
    $ret = TRUE;
    $mailer = new Message();
    try {
      $mailer->addTo($message['to']);
    } catch (Exception $e) {
      watchdog('gae_mailer', $e->getMessage(), array(), WATCHDOG_ERROR);
      $ret = FALSE;
    }
    try {
      $mailer->setHtmlBody($message['body']);
    } catch (Exception $e) {
      watchdog('gae_mailer', $e->getMessage(), array(), WATCHDOG_ERROR);
      $ret = FALSE;
    }
    try {
      $mailer->setSubject($message['subject']);
    } catch (Exception $e) {
      watchdog('gae_mailer', $e->getMessage(), array(), WATCHDOG_ERROR);
      $ret = FALSE;
    }
    try {
      $mailer->setSender($message['from']);
    } catch (Exception $e) {
       // $e;
       watchdog('gae_mailer', $e->getMessage(), array(), WATCHDOG_ERROR);
      $ret = FALSE;
    }
    //send the message actually
    try {
      $mailer->send();
    } catch (Exception $e) {
      // echo $e->getMessage();
      // echo $e->getCode(); 
      try {
        //try again with the default fallback email!
        $default_fallback = variable_get('gae_default_fallback_mail', default);
        $mailer->setSender($default_fallback);
        $mailer->send();
      } catch (Exception $e) {
        watchdog('gae_mailer', $e->getMessage(), array(), WATCHDOG_ERROR);
        $ret = FALSE;
      }
    }

    return $ret;
  }


  /**
   * Format Handler, currently does nothing except for stopping drupal from stripping my lovely HTML :)
   * @todo We might want to sanitize the message body later on
   *
   * @param $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return
   *   The formatted $message.
   */
  public function format(array $message)
  {
    if(is_array($message['body'])){
      reset($message['body']);
        $body = next($message['body']);
        foreach($message['body'] as $line){
          $body .= '<br/>'.$line;
        }
    }
    else{
      $body = $message['body'];
    }

    $message['body'] = $body;
    return $message;
  }
}